import java.util.*

class PoliBurger(val name: String)

class Juan : Observable() {

    val name = "Juan"

    fun cookBurger(name : String){
        var burger = PoliBurger(name)


        setChanged()
        notifyObservers(burger)
    }
}

class Andres : Observer{

    val name = "Andres"

    override fun update(o: Observable?, arg: Any?) {
        when (o){
            is   Juan -> {
                if (arg is PoliBurger){
                    println("$name esta sirviendo ${arg.name} cocinado por ${o.name}")
                }
            }
            else -> println(o?.javaClass.toString())
        }
    }
}


fun main(args : Array<String>){
    val bob = Juan()
    //Provide Bob and instance of Tina
    bob.addObserver(Andres())

    bob.cookBurger(" necesita conocer la PoliBurger")
}

